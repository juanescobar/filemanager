<?php
/**
 * Juan M. Escobar T. (http://www.juanescobar.org/)
 *
 * @link      http://rendimientos.sprbun.com/
 * @copyright Copyright (c) 2012 Juan M. Escobar T. (http://www.juanescobar.org)
 * @license   http://creativecommons.org/licenses/by-nc-nd/2.5/co/deed.es_ES New Creative Commons License
 */

namespace Archivos;
use Archivos\Model\Archivos;
use Archivos\Model\ArchivosTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
	public function getAutoloaderConfig()
	{
		return array(
				'Zend\Loader\ClassMapAutoloader' => array(
						__DIR__ . '/autoload_classmap.php',
				),
				'Zend\Loader\StandardAutoloader' => array(
						'namespaces' => array(
								__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
						),
				),
		);
	}

	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}
	
	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'Archivos\Model\ArchivosTable' =>  function($sm) {
					$tableGateway = $sm->get('ArchivosTableGateway');
					$table = new ArchivosTable($tableGateway);
					return $table;
				},
				'ArchivosTableGateway' => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new Archivos());
					return new TableGateway('files', $dbAdapter, null, $resultSetPrototype);
				},
			),
		);
	}
}