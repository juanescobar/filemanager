<?php
/**
 * Juan M. Escobar T. (http://www.juanescobar.org/)
 *
 * @link      http://rendimientos.sprbun.com/
 * @copyright Copyright (c) 2012 Juan M. Escobar T. (http://www.juanescobar.org)
 * @license   http://creativecommons.org/licenses/by-nc-nd/2.5/co/deed.es_ES New Creative Commons License
 */

namespace Archivos\Model;

use Zend\Db\TableGateway\TableGateway;

class ArchivosTable
{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function getArchivos($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function saveArchivos(Archivos $archivos)
	{
		$data = array(
			'name'		 => $archivos->name,
			'type'		 => $archivos->type,
			'size'		 => $archivos->size,
			'hash'		 => $archivos->hash,
			'fileupload' => $archivos->fileupload,
			'title' 	 => $archivos->title,
			'date'  	 => $archivos->date,
			'time'  	 => $archivos->time,
			'users_id'   => $archivos->users_id,
		);

		$id = (int)$archivos->id;
		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getArchivos($id)) {
				$this->tableGateway->update($data, array('id' => $id));
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteArchivos($id)
	{
		$this->tableGateway->delete(array('id' => $id));
	}
}