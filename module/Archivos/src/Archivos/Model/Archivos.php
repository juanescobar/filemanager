<?php
/**
 * Juan M. Escobar T. (http://www.juanescobar.org/)
 *
 * @link      http://rendimientos.sprbun.com/
 * @copyright Copyright (c) 2012 Juan M. Escobar T. (http://www.juanescobar.org)
 * @license   http://creativecommons.org/licenses/by-nc-nd/2.5/co/deed.es_ES New Creative Commons License
 */

namespace Archivos\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Archivos implements InputFilterAwareInterface
{
	public $id;
	public $name;
	public $type;
	public $size;
	public $hash;
	public $fileupload;
	public $title;
	public $date;
	public $time;
	public $users_id;
	protected $inputFilter;

	public function exchangeArray($data)
	{
		$this->id     	  = (isset($data['id'])) ? $data['id'] : null;
		$this->name       = (isset($data['name'])) ? $data['name'] : null;
		$this->type       = (isset($data['type'])) ? $data['type'] : null;
		$this->size       = (isset($data['size'])) ? $data['size'] : null;
		$this->hash    	  = (isset($data['hash'])) ? $data['hash'] : null;
		$this->fileupload = (isset($data['fileupload'])) ? $data['fileupload'] : null;
		$this->title 	  = (isset($data['title'])) ? $data['title'] : null;
		$this->date  	  = (isset($data['date'])) ? $data['date'] : null;
		$this->time  	  = (isset($data['time'])) ? $data['time'] : null;
		$this->users_id   = (isset($data['users_id'])) ? $data['users_id'] : null;
	}
	
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}
	
	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();
	
			$inputFilter->add($factory->createInput(array(
				'name'     => 'id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			)));
	
			$inputFilter->add($factory->createInput(array(
				'name'     => 'title',
				'required' => true,
				'validators' => array(
		            array(
		                'name' => 'not_empty',
		            ),
		            array(
		                'name' => 'string_length',
		                'options' => array(
		                    'min' => 1,
		                ),
		            ),
		        ),
			)));
	
			$inputFilter->add($factory->createInput(array(
				'name'     => 'fileupload',
				'required' => true,
			)));
			
			$this->inputFilter = $inputFilter;
		}
	
		return $this->inputFilter;
	}
}