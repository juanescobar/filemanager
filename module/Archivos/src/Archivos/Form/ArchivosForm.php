<?php
/**
 * Juan M. Escobar T. (http://www.juanescobar.org/)
 *
 * @link      http://rendimientos.sprbun.com/
 * @copyright Copyright (c) 2012 Juan M. Escobar T. (http://www.juanescobar.org)
 * @license   http://creativecommons.org/licenses/by-nc-nd/2.5/co/deed.es_ES New Creative Commons License
 */

namespace Archivos\Form;

use Zend\Form\Element;
use Zend\Form\Form;
use Zend\Filter;

class ArchivosForm extends Form
{
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('archivos');
		$this->setAttributes(array(
			'method' => 'post',
			'class'  => 'form-horizontal'
		));
		
		$id = new Element\Hidden('id');
		
		$title = new Element\Text('title');
		$title->setLabel('Nombre');
		
		$filesize = new Element\Hidden('MAX_FILE_SIZE');
		$filesize->setValue('3145728');
			   
		$fileupload = new Element\File('fileupload');
		$fileupload->setLabel('&nbsp')
		          ->setAttributes(array(
		          		'required' => true,
		          ));
		
		$submit  = new Element\Submit('submit');
		$submit->setValue('Submit')
			   ->setAttributes(array(
			   		'class' => 'btn btn-primary'
			   	));
		
		$this->add($id)
			 ->add($title)
			 ->add($filesize)
			 ->add($fileupload)
			 ->add($submit);

	}
}