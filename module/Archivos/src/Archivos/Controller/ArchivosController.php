<?php
/**
 * Juan M. Escobar T. (http://www.juanescobar.org/)
 *
 * @link      http://rendimientos.sprbun.com/
 * @copyright Copyright (c) 2012 Juan M. Escobar T. (http://www.juanescobar.org)
 * @license   http://creativecommons.org/licenses/by-nc-nd/2.5/co/deed.es_ES New Creative Commons License
 */

namespace Archivos\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Archivos\Model\Archivos;
use Archivos\Form\ArchivosForm;

class ArchivosController extends AbstractActionController
{
	protected $archivosTable;
	
	public function indexAction()
	{
		$form = new ArchivosForm();
		$form->get('submit')->setValue('Cargar');
		return new ViewModel(array(
			'archivos' => $this->getArchivosTable()->fetchAll(),
			'form' => $form,
		));
	}

	public function addAction()
	{
		$form = new ArchivosForm();
		$form->get('submit')->setValue('Agregar');
		
		$request = $this->getRequest();
		if ($request->isPost()) {
			$archivos = new Archivos();
			$form->setInputFilter($archivos->getInputFilter());

			$adapter = new \Zend\File\Transfer\Adapter\Http();
			$adapter->setDestination('data/upload/');
			$adapter->addValidator('Count', false, 1);
			$adapter->addValidator('MimeType', false, array(
				'application/vnd.ms-excel',
				'application/pdf',
				'application/zip',
			));
			$adapter->addValidator('Size', false, 3145728);
			
			if (!$adapter->receive()) {
				echo "debug (1): " . implode("\n", $adapter->getMessages());exit;
			} else {
				$dataPost = $request->getPost()->toArray();
				$dataPost['fileupload'] = $adapter->getFileName();
				$form->setData($dataPost);
				if ($form->isValid()) {
					$data = array(
						'id' 		 => $dataPost['id'],
						'name'		 => $adapter->getFileName('fileupload',false),
						'type'		 => $adapter->getMimeType(),
						'size'		 => $adapter->getFileSize(),
						'hash'		 => $adapter->getHash('md5'),
						'fileupload' => file_get_contents($adapter->getFileName()),
						'title'    	 => $dataPost['title'],
						'date' 	   	 => date('Y-m-d'),
						'time' 	   	 => date('H:i:s'),
						'users_id' 	 => 1,
					);
					$archivos->exchangeArray($data);
					$this->getArchivosTable()->saveArchivos($archivos);
				} else {
					echo "form isn't valid";
					exit;
				}
				// Delete file from destination folder
				unlink($adapter->getFileName());
				// Redirect to list of files
				return $this->redirect()->toRoute('archivos');
			}
		}
		return $this->redirect()->toRoute('archivos');
	}

	public function editAction()
	{
	}

	public function deleteAction()
	{
	}
	
	public function getArchivosTable()
	{
		if (!$this->archivosTable) {
			$sm = $this->getServiceLocator();
			$this->archivosTable = $sm->get('Archivos\Model\ArchivosTable');
		}
		return $this->archivosTable;
	}
}