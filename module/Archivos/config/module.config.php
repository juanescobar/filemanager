<?php
/**
 * Juan M. Escobar T. (http://www.juanescobar.org/)
 *
 * @link      http://rendimientos.sprbun.com/
 * @copyright Copyright (c) 2012 Juan M. Escobar T. (http://www.juanescobar.org)
 * @license   http://creativecommons.org/licenses/by-nc-nd/2.5/co/deed.es_ES New Creative Commons License
 */

return array(
	'controllers' => array(
		'invokables' => array(
			'Archivos\Controller\Archivos' => 'Archivos\Controller\ArchivosController',
		),
	),
	'router' => array(
		'routes' => array(
			'archivos' => array(
				'type'    => 'segment',
				'options' => array(
					'route'    => '/archivos[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'     => '[0-9]+',
					),
					'defaults' => array(
						'controller' => 'Archivos\Controller\Archivos',
						'action'     => 'index',
					),
				),
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'archivos' => __DIR__ . '/../view',
		),
	),
);